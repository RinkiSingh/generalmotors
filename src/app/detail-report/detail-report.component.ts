import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import * as _ from 'lodash';
import * as html2canvas from 'html2canvas';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import {VLSService} from '../services/vls.services';
import {response} from '../api-response/response';
import {environment} from '../../environments/environment';


export declare interface PriceObject {
  Value: number;
  Type: string;
  CurrencyID: string;
}


@Component({
  selector: 'general-motor-detail-report',
  templateUrl: './detail-report.component.html',
  styleUrls: ['./detail-report.component.scss']
})
export class DetailReportComponent implements OnInit {

  public displayAdditionalVehicleInfo = true;
  public displayGmMarketingInfo = true;
  public reportType = 'detail';
  public reportFor = 'customer';
  public vinNumber: string;
  public customer_company: string;
  public address: string;
  public salesConsultant: string;
  public data: any;
  public MSRP: PriceObject;
  public employeePrice: PriceObject;
  public supplierPrice: PriceObject;
  public invoice310: PriceObject;
  public listOfOptions: Array<any>;
  public tradeContact: any;

  constructor(public router: Router, private vlsService: VLSService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute
      .queryParams
      .subscribe(params => {
        this.vinNumber = params.vin;
        this.reportFor = params.view || 'customer';
        this.reportType = params.pageName === 'vehicledetail' || params.pageName === 'results' ? 'detail' : 'summary';
      });
    if (!window['data']) {
      /*this.vlsService.locateVehicleByVin(this.vinNumber).subscribe(response =>{
       })*/
      this.data = response.locateVehicle.ShowVehicleInventory.SearchByVIN;
      // this.data = response.locateVehiclesByRegion.ShowVehicleInventory.SearchByRegion;
    } else {
      this.data = JSON.parse(window['data']);
      console.log(this.data);
    }
    /*this.vlsService.getTradeContact(this.vinNumber).subscribe(response =>{
     })*/
    this.tradeContact = response.tradeContactInfo.response;
  }

  ngOnInit() {
    _.each(this.data, (data) => {
      data.PEG = _.find(data.Option, {FamilyCode: 'SPP'});
      data.primaryColor = _.find(data.Option, {FamilyCode: 'CCU'});
      data.trim = _.find(data.Option, {FamilyCode: 'ITC'});
      data.transmission = _.find(data.Option, {FamilyCode: 'TRN'});
      data.engine = _.find(data.Option, {FamilyCode: 'ENG'});
      data.MSRP = _.find(data.Price, {Type: 'MSRP'});
      data.employeePrice = _.find(data.Price, {Type: 'employeePrice'});
      data.supplierPrice = _.find(data.Price, {Type: 'supplierPrice'});
      data.invoice310 = _.find(data.Price, {Type: 'invoice310'});
      data.listOfOptions = [];
      _.each(data.Option, function (option) {
        data.listOfOptions.push(option.OptionCode);
      });
    });
  }

  /*printReport($event) {
   const win = window.open('', '_blank', 'location=yes,scrollbars=yes,status=yes');
   const divTag = document.getElementById('printArea');
   const newTag = document.createElement('div');
   newTag.innerHTML = '<div style="display: block;width: 100%;background: #134373;"><img src="/assets/images/gm_logo.gif" class="logo"></div>';
   divTag.insertBefore(newTag, divTag.firstChild);
   _.each(divTag.getElementsByClassName('no-print'), function (tag: HTMLElement) {
   tag.style.display = 'none';
   });
   _.each(divTag.getElementsByClassName('print'), function (tag: HTMLElement) {
   tag.style.display = 'inline-block';
   });
   const indecator: HTMLElement = <HTMLScriptElement>divTag.getElementsByClassName('mat-expansion-indicator')[0];
   indecator.style.display = 'none';
   html2canvas(document.getElementById('printArea')).then((canvas: any) => {
   const data = canvas.toDataURL();
   const docDefinition = {
   content: [{
   image: data,
   width: 500
   }]
   };
   pdfMake.createPdf(docDefinition).open({}, win);
   indecator.style.display = '';
   divTag.firstElementChild.remove();
   _.each(divTag.getElementsByClassName('no-print'), function (tag: HTMLElement) {
   tag.removeAttribute('style');
   });
   _.each(divTag.getElementsByClassName('print'), function (tag: HTMLElement) {
   tag.style.display = 'none';
   });
   });
   }*/

  printReport(event) {
    const divTag = document.getElementById('printArea');
    const newTag = document.createElement('div');
    newTag.innerHTML = '<div style="display: block;width: 100%;background: #134373;"><img src="/assets/images/gm_logo.gif" class="logo"></div>';
    divTag.insertBefore(newTag, divTag.firstChild);
    _.each(divTag.getElementsByClassName('no-print'), function (tag: HTMLElement) {
      tag.style.display = 'none';
    });
    _.each(divTag.getElementsByClassName('print'), function (tag: HTMLElement) {
      tag.style.display = 'inline-block';
    });
    const indecator: HTMLElement = <HTMLScriptElement>divTag.getElementsByClassName('mat-expansion-indicator')[0];
    (document.getElementsByClassName('report-detail-top')[0]).setAttribute('style', 'display: none');
    const innerHtml = document.getElementById('printArea').innerHTML;
    let printMailTemplate: string = '<html><head>' +
      '<link rel="stylesheet" href="../../assets/print.css" media="print">' +
      '<link rel="stylesheet" href="../../assets/indigo-pink.css">' +
      '</head>' +
      '<body>{{content}}</body></html>';
    printMailTemplate = printMailTemplate.replace('{{content}}', innerHtml);

    const printWin = window.open();
    printWin.document.write(printMailTemplate);
    (document.getElementsByClassName('report-detail-top')[0]).removeAttribute('style');
    printWin.focus();
    setTimeout(() => {
      indecator.style.display = '';
      divTag.firstElementChild.remove();
      _.each(divTag.getElementsByClassName('no-print'), function (tag: HTMLElement) {
        tag.removeAttribute('style');
      });
      _.each(divTag.getElementsByClassName('print'), function (tag: HTMLElement) {
        tag.style.display = 'none';
      });
      printWin.print();
      printWin.close();
    }, 300);
  }

  showVinDetail() {
    window.open(`${environment.baseURL}/${environment.vinDetail}`, '_blank', 'location=yes,scrollbars=yes,status=yes');
  }
}
