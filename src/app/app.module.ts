import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SearchByLocationComponent } from './locate-dealer/search-by-location/search-by-location.component';
import { SearchResultComponent } from './locate-dealer/search-result/search-result.component';
import { QuickLinksComponent } from './quick-links/quick-links.component';
import { LocateByVehicleComponent } from './locate-vehicle/locate-by-vehicle/locate-by-vehicle.component';
import { QuickLocateComponent } from './quick-locate/quick-locate.component';
import { SelectOptionComponent } from './locate-vehicle/select-option/select-option.component';
import { FooterComponent } from './footer/footer.component';
import { SelectedOptionsComponent } from './locate-vehicle/selected-options/selected-options.component';

import { PickListModule } from 'primeng/primeng';
import { AccordionModule, MenuItem} from 'primeng/primeng';
import { SearchCriteriaComponent } from './search-criteria/search-criteria.component';
import { AlertComponent } from './modal/alert/alert.component';
import { SearchBacComponent } from './modal/search-bac/search-bac.component';
import { SearchVehicleResultComponent } from './locate-vehicle/search-vehicle-result/search-vehicle-result.component';
import { VehicleOptionsComponent } from './vin-detail/vehicle-options/vehicle-options.component';
import { StandardPaymentEstimatorComponent } from './standard-payment-estimator/standard-payment-estimator.component';
import { VinSpecificationComponent } from './vin-detail/vin-specification/vin-specification.component';
import { VinDetailComponent } from './vin-detail/vin-detail.component';
import { OtherLinksComponent } from './other-links/other-links.component';
import { DetailReportComponent } from './detail-report/detail-report.component';
import { InfoNavbarComponent } from './info-navbar/info-navbar.component';
import { CompareVehicleComponent } from './compare-vehicle/compare-vehicle.component';
import { OwningDealerComponent } from './table/owning-dealer/owning-dealer.component';
import { TradingPartnerComponent } from './trading-partner/trading-partner.component';
import { DealerDetailComponent } from './dealer-detail/dealer-detail.component';
import { VSSService } from './services/vss.services';
import { VISService } from './services/vis.services';
import { VLSService } from './services/vls.services';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {PrintService} from './services/print.services';
import { HelpfulLinksComponent } from './helpful-links/helpful-links.component';


@NgModule({
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		PickListModule,
		AccordionModule,
    HttpModule
	],
	declarations: [
		AppComponent,
		NavbarComponent,
		SearchByLocationComponent,
		SearchResultComponent,
		QuickLinksComponent,
		LocateByVehicleComponent,
		QuickLocateComponent,
		SelectOptionComponent,
		FooterComponent,
		SelectedOptionsComponent,
		SearchCriteriaComponent,
		AlertComponent,
		SearchBacComponent,
		SearchVehicleResultComponent,
		VehicleOptionsComponent,
		StandardPaymentEstimatorComponent,
		VinSpecificationComponent,
		VinDetailComponent,
		OtherLinksComponent,
		DetailReportComponent,
		InfoNavbarComponent,
		CompareVehicleComponent,
		OwningDealerComponent,
		TradingPartnerComponent,
		DealerDetailComponent,
		HelpfulLinksComponent
	],
	entryComponents: [
		SelectedOptionsComponent,
		AlertComponent,
		SearchBacComponent
	],
	providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    { provide: 'Window',  useValue: window },
	  VSSService,
    VISService,
    VLSService,
    PrintService
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
