import {Component, Input, OnInit} from '@angular/core';
import * as _ from 'lodash';
@Component({
	selector: 'general-motor-vehicle-options',
	templateUrl: './vehicle-options.component.html',
	styleUrls: ['./vehicle-options.component.scss']
})
export class VehicleOptionsComponent implements OnInit {

	@Input() options: any;
	public data: any;
	public price: any;

	constructor() {
	}

	ngOnInit() {
	  const that = this;
		this.data = _.groupBy(this.options, 'OptionType');
		_.each(this.data['Chargable Options'], (option, index) => {
      that.data['Chargable Options'][index].MSRP = _.find(option.Price, {Type: 'MSRP'})
      that.data['Chargable Options'][index].invoice = _.find(option.Price, {Type: 'Invoice'})
    });
	}

}
