import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {VLSService} from '../services/vls.services';
import {response} from '../api-response/response';

import {environment} from '../../environments/environment';

@Component({
  selector: 'general-motor-vin-detail',
  templateUrl: './vin-detail.component.html',
  styleUrls: ['./vin-detail.component.scss']
})
export class VinDetailComponent implements OnInit {

  public displayAdditionalVehicleInfo: boolean;
  public displayGmMarketingInfo: boolean;
  public selectedView = 'customer';
  public data: any;
  public response: any;
  public incompleteData = [];
  public tradeContact: any;

  constructor(private activatedRoute: ActivatedRoute, private vlsService: VLSService) {
    /*if (window['data']) {
     this.data = JSON.parse(window['data']);
     this.tradeContact = response.tradeContactInfo.response;
     } else {
     this.activatedRoute.queryParams.subscribe((params: Params) => {
     /!*this.vlsService.locateVehicleByVin(params.vin).subscribe(response => {
     });*!/
     // this.response = response.locateVehicleByVin;
     this.response = response.locateVehicle;
     this.data = this.response.ShowVehicleInventory.SearchByVIN[0];
     this.tradeContact = response.tradeContactInfo.response;
     });
     }*/
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      /*this.vlsService.locateVehicleByVin(params.vin).subscribe(response => {
       });*/
      // this.response = response.locateVehicleByVin;
      this.response = response.locateVehicle;
      this.data = this.response.ShowVehicleInventory.SearchByVIN[0];
      this.tradeContact = response.tradeContactInfo.response;
    });
  }

  ngOnInit() {
    this.displayAdditionalVehicleInfo = true;
    this.displayGmMarketingInfo = true;
  }

  addToTradingPartner() {

  }

  changeView(event) {
    this.selectedView = event.value;
    /*		this.dataSource = new MatTableDataSource(this.data);
     this.incompleteDataSource = new MatTableDataSource(this.incompleteData);*/
  }

  emailCustomer() {

  }

  viewReport() {
    window.open(`${environment.baseURL}/${environment.viewReport}?view=dealer&pageName=vehicledetail&vin=${this.data.VinNumber}`, '_blank', 'location=yes,scrollbars=yes,status=yes');
  }

  printReport($event) {

  }
}
