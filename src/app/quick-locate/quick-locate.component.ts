import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {VLSService} from '../services/vls.services';
import {response} from '../api-response/response';
import {environment} from '../../environments/environment';


@Component({
  selector: 'general-motor-quick-locate',
  templateUrl: './quick-locate.component.html',
  styleUrls: ['./quick-locate.component.scss']
})
export class QuickLocateComponent implements OnInit, OnChanges {

  @Input() clearData: boolean;

  @Output() quickLocateError = new EventEmitter<any>();
  public number: string;
  public quickForm: FormGroup;
  public seasons = [
    'VIN',
    'Stock #',
    'Order #',
  ];

  constructor(private vlsService: VLSService) {
    this.quickForm = new FormGroup({
      searchBy: new FormControl('VIN', Validators.required),
      value: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (changes.clearData.currentValue) {
      this.quickForm.controls['searchBy'].setValue('VIN');
      this.quickForm.controls['value'].setValue('');
      this.quickForm.controls['value'].markAsUntouched();
    }
  }

  checkNumber(event){
    return (event.charCode >= 48 && event.charCode <= 57) ||
      (event.charCode >= 97 && event.charCode <= 122)
      || (event.charCode >= 65 && event.charCode <= 90);
  }

  getQuickLocateData(form) {
    if (form.valid) {
      const {searchBy, value} = form.value;
      switch (searchBy) {
        case 'Stock #': {
          break;
        }
        case 'Order #': {
          break;
        }
        default: {
          /*this.vlsService.locateVehicleByVin(value).subscribe(response => {
           });*/
          if (response.locateVehicleByVin.ShowVehicleInventory.Status.Code === '30000') {
            window.open(`${environment.baseURL}/${environment.vinDetail}?vin=${value}`, '_blank', 'location=yes,scrollbars=yes,status=yes');
          } else {
            this.quickLocateError.emit({error: response.locateVehicleByVin.ShowVehicleInventory.Status.Message})
          }
          break;
        }
      }
    }
  }


  onOptionChange(event) {
    this.clearData = false;
    this.quickForm.controls.value.reset();
  }

}
