import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SelectedOptionsComponent} from '../selected-options/selected-options.component';
import * as _ from 'lodash';
import {response} from '../../api-response/response';

@Component({
	selector: 'general-motor-select-option',
	templateUrl: './select-option.component.html',
	styleUrls: ['./select-option.component.scss']
})
export class SelectOptionComponent implements OnInit, OnChanges {

	@Input() showOptions: boolean;
	@Input() modelOptionGroup: any;

	selectOptions = {
    primaryColor: {
      data: []
    },
    trans: {
      data: []
    },
    trim: {
      data: []
    },
    PEG: {
      data: []
    },
    engine: {
      data: []
    },
    options: {
      data: []
    }
  };
	totalWant = 0;
	totalDoNot = 0;
	selectedIndex = 0;
	searchedData = [];
	searchString = '';
	objectKeys = Object.keys;

	constructor(public dialog: MatDialog) {
	}

	openDialog(): void {
		const dialogRef = this.dialog.open(SelectedOptionsComponent, {
			width: '620px',
			data: {selectOptions: this.selectOptions}
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');

		});
	}

	ngOnInit() {
	}

	ngOnChanges(changes) {
		this.showOptions = changes.showOptions && changes.showOptions.currentValue;
		if (changes.modelOptionGroup.currentValue && this.modelOptionGroup.OptionGroup) {
			this.modelOptionGroup.OptionGroup.forEach(function (item, index) {
				switch (item.OptionGroupCode) {
					case 'CCU' : {
						this.selectOptions.primaryColor.data = [];
						this.selectOptions.primaryColor.data = item.OptionList.Option;
						break;
					}
					case 'TRN' : {
						this.selectOptions.trans.data = [];
						this.selectOptions.trans.data = item.OptionList.Option;
						break;
					}
					case 'ITC' : {
						this.selectOptions.trim.data = [];
						this.selectOptions.trim.data = item.OptionList.Option;
						break;
					}
					case 'PEG' : {
						this.selectOptions['PEG'].data = [];
						this.selectOptions['PEG'].data = item.OptionList.Option;
						break;
					}
					default :
						break;
				}
			}.bind(this));
		}
	}

	applyFilter(search) {
		this.searchedData = [];
		if (search !== '') {
			const regex = new RegExp(search, 'i');
			const key = Object.keys(this.selectOptions)[this.selectedIndex];
			_.each(this.selectOptions[key].data, function (data) {
				if ((data.OptionName.search(regex) !== -1)) {
					this.searchedData.push(data);
				}
			}.bind(this));
		}
	}

	tabChanged(event) {
		this.selectedIndex = event.index;
		this.searchedData = [];
		this.searchString = '';
	}

	selectWant(event, item, header) {
		if (event.checked) {
			item.doNot = false;
		}
		this.getTotalWantAndDoNot(header);
	}

	selectDoNot(event, item, header) {
		if (event.checked) {
			item.want = false;
		}
		this.getTotalWantAndDoNot(header);
	}

	getTotalWantAndDoNot(header) {
		let totalWant = 0;
		let totalDoNot = 0;
		this.selectOptions[header].totalWant = _.filter(this.selectOptions[header].data, {want: true}).length;
		this.selectOptions[header].totalDoNot = _.filter(this.selectOptions[header].data, {doNot: true}).length;
		_.forEach(this.selectOptions, function (data) {
			totalWant += data.totalWant || 0;
			totalDoNot += data.totalDoNot || 0;
		}.bind(this));
		this.totalDoNot = totalDoNot;
		this.totalWant = totalWant;
	}

}
