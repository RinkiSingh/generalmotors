import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import * as _ from 'lodash';
import {VSSService} from '../../services/vss.services';

import {response} from '../../api-response/response';
import {VLSService} from '../../services/vls.services';
import {AlertComponent} from '../../modal/alert/alert.component';
import {MatDialog} from '@angular/material';
import {SearchBacComponent} from '../../modal/search-bac/search-bac.component';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'general-motor-locate-by-vehicle',
  templateUrl: './locate-by-vehicle.component.html',
  styleUrls: ['./locate-by-vehicle.component.scss']
})
export class LocateByVehicleComponent implements OnInit, AfterViewInit {

  public searchVehicle: FormGroup;
  public years = [];
  public allYearMakeModel = [];
  public allMake = [];
  public makes = [];
  public models = [];
  public bodyStyles: object;
  public allSelected = false;
  public checkedValue = [];
  public selectedBodyStyle = [];
  public maxMSRP = 0;
  public eventCode: any;
  public maxRecord = 20;
  public result = false;
  public searchedData: any;
  public additionalData: any;
  public recordPerPageArray = [10, 20, 30, 40, 50];
  public earliestEventCode = [{
    code: 3000,
    value: 'Order accepted by Production Control'
  }, {
    code: 3800,
    value: 'Produced'
  }, {
    code: 5000,
    value: 'Delivered to Dealer'
  }];
  public selectedYear: any;
  public selectedMake: any;
  public selectedModel: any;
  public clearData: boolean;
  public displayError: string;
  public selectedSearchOptions: Array<any>;
  public sourceStates = [];
  public targetStates = [];
  public inValidRadius: any;
  public states: Array<any>;
  public errorMessage: string;
  public searchCriteria: any;
  public selectBodyError: boolean;
  public selectStateError: boolean;
  public selectedSearch: any;
  public showSelectionOption = false;
  public modelOptionGroups: any;
  public vehicleSearchType = [{
    value: 'Include CTP Vehicles',
    checked: false
  }, {
    value: 'Include Sold/Fleet Vehicles whose age is > 30 days outside My Inventory',
    checked: false
  }, {
    value: 'Include Demo Vehicles',
    checked: false
  }, {
    value: 'Include Package Vehicles outside of my Inventory',
    checked: false
  }, {
    value: 'Display Option Level Pricing',
    checked: false
  }, {
    value: 'Display Total Cash Allowance',
    checked: false
  }];

  constructor(public dialog: MatDialog,
              fb: FormBuilder,
              private titleService: Title,
              private vssService: VSSService,
              private vlsService: VLSService) {

    this.searchVehicle = fb.group({
      yearMakeModel: fb.group({
        year: new FormControl('', Validators.required),
        make: new FormControl('', Validators.required),
        model: new FormControl('', Validators.required),
      }),
      excludeBAC: fb.group({
        value: new FormControl(null),
        bacs: new FormArray([
          new FormControl({value: '', disabled: true}),
          new FormControl({value: '', disabled: true}),
          new FormControl({value: '', disabled: true}),
          new FormControl({value: '', disabled: true}),
          new FormControl({value: '', disabled: true}),
        ])
      }),
      selectedOption: fb.group({
        value: new FormControl('Zip Code'),
        'Zip Code': fb.group({
          code: new FormControl(97214, Validators.required),
          radius: new FormControl(5, Validators.required)
        }),
        City: fb.group({
          city: new FormControl({value: '', disabled: true}),
          state: new FormControl({value: '', disabled: true}),
          radius: new FormControl({value: '', disabled: true})
        }),
        bacRadius: new FormControl({value: '', disabled: true}),
        BAC: fb.group({
          number: new FormArray([
            new FormControl({value: '', disabled: true}),
            new FormControl({value: '', disabled: true}),
            new FormControl({value: '', disabled: true}),
            new FormControl({value: '', disabled: true}),
            new FormControl({value: '', disabled: true}),
          ])
        })
      })
    });
    /*this.vssService.getStates().subscribe(response => {
     this.states = response;
     this.sourceStates = response;
     });*/
    this.sourceStates = response.getStates;
    this.targetStates = [];
    this.sourceStates = _.sortBy(this.sourceStates, 'name');
  }

  ngOnInit() {
    //call getSates API
    /*this.vssService.getStates().subscribe(response => {
     debugger;
     });*/
    this.states = response.getStates;
    // call GetAllYears api
    /*this.vssService.getAllYears().subscribe(response => {
     this.years = response.ModelYears;
     });*/
    this.years = response.getAllYears.ModelYears;
    this.selectedYear = this.years[this.years.length - 1].Year;
    // this.searchVehicle.controls.year.setValue(this.years[this.years.length - 1].Year);
    // call GetAllMakes api
    /*this.vssService.getAllMakes(this.searchVehicle.controls.year.value).subscribe(response => {
     this.makes = response.ModelYears[0].MakeList.Make;
     })*/
    this.makes = response.getAllMakes.ModelYears[0].MakeList.Make;
    // this.allYearMakeModel = response.getAllYearMakeModel.ModelYears[this.years.length - 1].MakeList.Make;
    this.eventCode = _.find(this.earliestEventCode, {code: 5000});
    if (!this.selectedMake) {
      this.searchVehicle.controls.yearMakeModel['controls'].model.disable();
    }
  }

  ngAfterViewInit() {
    document.getElementsByClassName('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only')[0].setAttribute('icon', 'fa-angle-double-right');
    document.getElementsByClassName('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only')[1].setAttribute('style', 'display:none');
    document.getElementsByClassName('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only')[2].setAttribute('icon', 'fa-angle-double-left');
    document.getElementsByClassName('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only')[3].setAttribute('style', 'display:none');
  }

  bodyStyleSelected(event, index) {
    const selectOptionPromise = [];
    if (event.checked) {
      this.selectedBodyStyle.push(event.source.value);
    } else {
      this.selectedBodyStyle.splice(this.selectedBodyStyle.indexOf(event.source.value), 1);
    }
    this.showSelectionOption = false;
    _.each(this.selectedBodyStyle, function (value, i) {
      selectOptionPromise.push(new Promise((resolve, reject) => {
        /*this.vssService.getModelOptionGroup(this.selectedYear, this.selectedMake, event.source.value.modelId).subscribe(response => {
         resolve(response);
         });*/
      }));
    }.bind(this));
    Promise.all(selectOptionPromise).then(function (values) {

    }.bind(this));
    this.modelOptionGroups = this.selectedBodyStyle.length === 0 ? [] : response.getModelOptionGroup;
    this.showSelectionOption = true;
    this.checkedValue[index] = event.checked;
    this.allSelected = this.selectedBodyStyle.length === this.bodyStyles[this.selectedModel.name].length;
    this.selectBodyError = !this.selectedBodyStyle.length;
  }

  changeRadio(event) {
    _.each(this.searchVehicle.controls['selectedOption']['controls'], (control: any, index) => {
      if (event.value) {
        if (event.value === index) {
          if (index === 'bacRadius') {
            control.setValidators([Validators.required, Validators.max(500)]);
            control.enable();
          } else {
            _.forEach(control.controls, (value, field) => {
              if (field === 'radius') {
                value.setValidators([Validators.required, Validators.max(500)]);
              } else if (field === 'number') {
                _.each(value.controls, function (number, numberIndex) {
                  if (!numberIndex) {
                    number.validator = Validators.required;
                  }
                  number.enable();
                });
              } else {
                value.validator = Validators.required;
              }
              value.enable();
              value.markAsTouched();
            });
          }
        } else {
          if (index === 'bacRadius') {
            control.clearValidators();
            control.markAsUntouched();
            control.disable();
          } else {
            _.forEach(control.controls, (value) => {
              value.clearValidators();
              control.markAsUntouched();
              value.disable();
            });
          }
        }
      }
    });
  }

  changedRadio(event, value) {
    if (this.searchVehicle.controls.excludeBAC.value.value) {
      event.preventDefault();
      const dialogRef = this.dialog.open(AlertComponent, {
        data: {field: `Search By ${value}`}
      });
    }
  }

  checkNumber(event, field) {
    if (field === 'radius') {
      return event.charCode >= 48 && event.charCode <= 57 && event.target.value.length < 3;
    }
    return event.charCode >= 48 && event.charCode <= 57;
  }


  validateRadius(value) {
    this.inValidRadius = (value > 501);
  }

  checkValue(event) {
    const selectedOption = this.searchVehicle.controls.selectedOption.value.value;
    if (selectedOption === 'BAC' || selectedOption === 'My Trading Partner') {
      this.searchVehicle.controls.excludeBAC.value.value = false;
      event.source.checked = false;
      const dialogRef = this.dialog.open(AlertComponent, {
        data: {field: `Search By ${selectedOption}`}
      });
    } else {
      _.each(this.searchVehicle.controls.excludeBAC['controls'].bacs.controls, function (value, field) {
        const validation = [Validators.minLength(6), Validators.maxLength(6)];
        if (event.source.checked) {
          if (!field) {
            validation.unshift(Validators.required);
          }
          value.setValidators(validation);
          value.enable();
        } else {
          value.disable();
        }
      });
    }
  }

  displayQuickLocateError(event) {
    this.displayError = event.message;
  }

  editSearchCriteria() {
    this.result = false;
    this.titleService.setTitle('Locate Vehicle');
  }

  getData(event, formData, list) {
    this.selectBodyError = !this.selectedBodyStyle.length;
    this.selectStateError = this.searchVehicle.controls.selectedOption.value.value === 'state' && !this.targetStates.length;
    if (formData.valid && !this.selectBodyError && !this.selectStateError) {
      event.preventDefault();
      this.selectedSearchOptions = _.filter(this.vehicleSearchType, function (search) {
        return search.checked;
      });
      this.selectedSearchOptions.unshift({value: 'My Inventory'});
      this.result = true;
      const {year, make, model} = formData.value.yearMakeModel;
      if (formData.value.selectedOption.value === 'state') {
        formData.value.selectedOption[formData.value.selectedOption.value] = this.targetStates;
      }
      this.searchCriteria = {
        year: year,
        make: make,
        model: model,
        bodyStyle: this.selectedBodyStyle,
        searchedCriteria: this.selectedSearchOptions,
        earliestEventCode: this.eventCode,
        maxMSRP: this.maxMSRP,
        additionalCriteria: formData.value
      };
      this.selectedSearch = this.searchVehicle.value.selectedOption[this.searchVehicle.value.selectedOption.value];
      /*this.vlsService.locateVehiclesBySingleVendor(BAC, makeCode, modelCode, 5, year).subscribe(response =>{
       });*/
      this.searchedData = [];
      switch (this.searchVehicle.value.selectedOption.value) {
        case 'City': {
          const {city, state, radius} = formData.value.selectedOption[formData.value.selectedOption.value];
          /*this.vlsService.locateVehiclesByPostalCode(city, make.MakeID, model, radius, state, year).subscribe(response => {
           });*/
          this.result = true;
          if (response.locateVehiclesByCity.ShowVehicleInventory.Status.Code === '30000') {
            this.additionalData = response.locateVehiclesByCity.ShowVehicleInventory.SearchByCity;
          } else {
            this.errorMessage = response.locateVehiclesByPostalCode.ShowVehicleInventory.Status.Message;
            this.searchedData = [];
            this.additionalData = [];
          }
          break;
        }
        case 'bacRadius': {
          break;
        }
        case 'BAC': {
          const BAC = _.filter(formData.value.selectedOption.BAC.number, function (number) {
            return number !== '';
          });
          // const {city, state, radius} = formData.value.selectedOption[formData.value.selectedOption.value];
          /*this.vlsService.locateVehiclesByVendor(BAC, makeCode, modelCode, 5, year).subscribe(response => {
           });*/
          this.result = true;
          if (response.locateVehiclesByVendor.ShowVehicleInventory.Status.Code === '30000') {
            this.additionalData = response.locateVehiclesByVendor.ShowVehicleInventory.SearchByMultipleVendor;
          } else {
            this.errorMessage = response.locateVehiclesByVendor.ShowVehicleInventory.Status.Message;
            this.searchedData = [];
            this.additionalData = [];
          }
          break;
        }
        case 'state': {
          const selectedStates = [];
          this.targetStates.filter((state) => {
            selectedStates.push(state.stateId);
          });
          const states = selectedStates.toString();
          // const {city, state, radius} = formData.value.selectedOption[formData.value.selectedOption.value];
          /*this.vlsService.locateVehiclesByRegion(make.MakeID, model.modelId, 5, states, year).subscribe(response => {
           });*/
          this.result = true;
          if (response.locateVehiclesByRegion.ShowVehicleInventory.Status.Code === '30000') {
            this.additionalData = response.locateVehiclesByRegion.ShowVehicleInventory.SearchByRegion;
          } else {
            this.errorMessage = response.locateVehiclesByRegion.ShowVehicleInventory.Status.Message;
            this.searchedData = [];
            this.additionalData = [];
          }
          break;
        }
        default : {
          const {code, radius} = formData.value.selectedOption[formData.value.selectedOption.value];
          /*this.vlsService.locateVehiclesByPostalCode(make.MakeID, model.ModelID, code, radius, year).subscribe(response => {
           });*/
          this.result = true;
          if (response.locateVehiclesByPostalCode.ShowVehicleInventory.Status.Code === '30000') {
            this.additionalData = response.locateVehiclesByPostalCode.ShowVehicleInventory.SearchByPostalCode;
          } else {
            this.errorMessage = response.locateVehiclesByPostalCode.ShowVehicleInventory.Status.Message;
            this.searchedData = [];
            this.additionalData = [];
          }
          break;
        }
      }
    } else {
      if (!this.searchVehicle.controls.yearMakeModel.valid || this.selectBodyError) {
        window.scrollTo(0, 0);
      } else if (!this.searchVehicle.controls.selectedOption.valid || this.selectStateError) {
        window.scrollBy(0, 500);
      }
    }
  }

  moveToTarget(event) {
    if (this.targetStates.length > 5) {
      this.sourceStates = this.sourceStates.concat(this.targetStates.splice(5, this.targetStates.length));
    }
    this.sourceStates = _.sortBy(this.sourceStates, 'name');
    this.targetStates = _.sortBy(this.targetStates, 'name');
    this.selectStateError = !this.targetStates.length;
  }

  moveToSource($event) {
    this.sourceStates = _.sortBy(this.sourceStates, 'name');
    this.selectStateError = !this.targetStates.length;
  }

  onClearData(event) {
    window.scrollTo(0, 0);
    this.clearData = true;
    this.searchVehicle.reset();
    this.selectedBodyStyle = [];
    this.allSelected = false;
  }

  onMakeChange(event) {
    this.models = [];
    this.bodyStyles = {};
    this.selectedBodyStyle = [];
    this.searchVehicle.controls.yearMakeModel['controls'].model.reset();
    this.searchVehicle.controls.yearMakeModel['controls'].model.enable();
    /*this.vssService.getAllModels(this.searchVehicle.controls.year.value, event.value).subscribe(response =>{
     });*/
    const model = response.getAllModels.ModelYears[0].MakeList.Make[0].ModelList.Model;
    _.each(model, function (value, valueIndex) {
      const modelValue = value.Description.split(': ')[0];
      const index = _.findIndex(this.models, {name: modelValue});
      // const index = this.models.indexOf(modelValue);
      if (index === -1) {
        // this.models.push(value.Description.split(': ')[0]);
        this.models.push({name: value.Description.split(': ')[0], modelId: value.ModelID});
      }
      if (Object.keys(this.bodyStyles).indexOf(modelValue) === -1) {
        this.bodyStyles[modelValue] = [];
      }
      this.bodyStyles[modelValue].push({
        modelId: value.ModelID,
        style: `${value.ModelID} - ${value.Description.split(': ')[1]}`
      });
    }.bind(this));
    this.models = _.sortBy(this.models, ['name']);
  }

  onModelChange(event) {
    this.checkedValue = new Array(this.bodyStyles[event.value.name].length);
    this.selectedBodyStyle = [];
    this.allSelected = false;
  }

  onYearChange(event) {
    /*this.vssService.getAllMakes(event.value).subscribe(response =>{
     });*/
    this.searchVehicle.controls.yearMakeModel['controls'].make.reset();
    this.searchVehicle.controls.yearMakeModel['controls'].model.reset();
    this.searchVehicle.controls.yearMakeModel['controls'].model.disable();
    this.allMake = response.getAllMakes.ModelYears[0].MakeList.Make;
    this.selectedBodyStyle = [];
  }

  openSearchByLocation() {
    const dialogRef = this.dialog.open(SearchBacComponent, {
      width: '60%',
      data: {
        quickLinks: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  selectAll(event) {
    const that = this;
    this.selectedBodyStyle = [];
    this.selectBodyError = !event.checked;
    if (event.checked) {
      this.allSelected = true;
      this.selectedBodyStyle = _.concat(this.selectedBodyStyle, this.bodyStyles[this.selectedModel.name]);
      this.modelOptionGroups = response.getModelOptionGroup;
      this.showSelectionOption = true;
    } else {
      this.allSelected = false;
      this.modelOptionGroups = [];
      this.showSelectionOption = false;
    }
    _.each(this.checkedValue, function (item, index) {
      that.checkedValue[index] = event.checked;
    });
  }
}
