import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';

export declare interface PriceObject {
	Value: number;
	Type: string;
	CurrencyID: string;
}

@Component({
	selector: 'general-motor-standard-payment-estimator',
	templateUrl: './standard-payment-estimator.component.html',
	styleUrls: ['./standard-payment-estimator.component.scss']
})
export class StandardPaymentEstimatorComponent implements OnInit {

	@Input() data: PriceObject[];
	public form: FormGroup;

	constructor() {
		this.form = new FormGroup({
			finalPrice: new FormControl('', [Validators.required, Validators.max(120), Validators.min(1)]),
			term: new FormControl('', Validators.required),
		});
	}

	ngOnInit() {
		this.form.controls.finalPrice.setValue(_.find(this.data, {Type: 'MSRP'}).Value);
	}

	calculatePayment(options) {
	}

	getFinalPriceError() {
		return this.form.controls['finalPrice'].hasError('required') ? 'You must enter a value' :
			this.form.controls['finalPrice'].hasError('min') ? 'Not a valid email' :
				'';
	}
	/*getErrorMessage() {
    return this.form.controls['finalPrice'].hasError('required') ? 'You must enter a value' :
      this.email.hasError('email') ? 'Not a valid email' :
        '';
  }*/
}
