import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { environment } from '../../environments/environment';
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'general-motor-info-navbar',
  templateUrl: './info-navbar.component.html',
  styleUrls: ['./info-navbar.component.scss']
})
export class InfoNavbarComponent implements OnInit {

  @Input() showHelp: boolean;
  @Input() showPrint: boolean;
  @Output() printClicked = new EventEmitter<any>();
  public environment = environment;

  constructor(private router: Router, private titleService: Title) {
  }

  ngOnInit() {
  }

  print(event) {
    this.printClicked.emit(event);
  }

  openHelpfulLinks() {
    let title = this.titleService.getTitle();
    title = title.replace(/ /g, '_').toUpperCase();
    window.open(`${environment.baseURL}/${environment.helpfulLink}?pageId=${title}`, '_blank', 'location=true');
  }

}
