import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';

@Component({
	selector: 'general-motor-owning-dealer',
	templateUrl: './owning-dealer.component.html',
	styleUrls: ['./owning-dealer.component.scss']
})
export class OwningDealerComponent implements OnInit {

	@Input() data: any;
	@Input() vinDetail: boolean;
	dataSource: any;
	public displayedColumns = ['tradeContact', 'name', 'title', 'phone', 'ext', 'fax', 'email', 'preferredModeOfContact', 'textMessage', 'additionalInfo'];

	constructor() {
	}

	ngOnInit() {
		if (this.vinDetail) {
			this.displayedColumns.push('gmMarketingInfo');
		}
		this.dataSource = new MatTableDataSource(this.data);
	}

}
