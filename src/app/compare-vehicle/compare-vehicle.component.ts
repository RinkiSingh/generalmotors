import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';

import * as _ from 'lodash';
import * as html2canvas from 'html2canvas';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
import {response} from '../api-response/response';
import {PrintService} from "../services/print.services";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'general-motor-compare-vehicle',
  templateUrl: './compare-vehicle.component.html',
  styleUrls: ['./compare-vehicle.component.scss']
})
export class CompareVehicleComponent implements OnInit {
  public vehicleComairArr: Array<any>;
  public vehicles: Array<any>;
  public vin: any;
  public objectKeys = Object.keys;
  public header: any;
  public response: any;
  public comparision: any;
  public comparisionOptions: any;
  public selectedView = 'Customer';
  public differentOptions = true;
  public similarOptions = true;

  constructor(private activatedRoute: ActivatedRoute, private printService: PrintService) {
  }

  ngOnInit() {
    this.activatedRoute
      .queryParams
      .subscribe(params => {
        this.vin = params['vin'];
      });
    this.response = response.vehicleComparison;
    this.vehicles = this.response.responseRoot1;
    this.comparision = this.response.responseRoot2;
    this.comparisionOptions = _.groupBy(this.comparision, {optionType: 'Different Options'});
    this.header = [{
      headerTitle: '2017 Chevrolet Malibu 1ZD69 - LT HF127437'
    }, {
      headerTitle: '2017 Chevrolet Malibu 1ZD69 - LT HF127437'
    }];
    this.vehicleComairArr = [
      {
        MSRP: 27035,
        PEG: '1LT - 1LT Preferred Equipment Group',
        PrimaryColor: 'GB8 - Mosaic Black Metallic',
        Trim: 'H1T - Jet Black Cloth Interior Trim',
        Engine: 'LFV - Engine 4 cyl, 1.5L, Turbo',
        Transmission: 'MNH - Transmission, 6speed, automatic'
      },
      {
        MSRP: 27035,
        PEG: '1LT - 1LT Preferred Equipment Group',
        PrimaryColor: 'GB8 - Mosaic Black Metallic',
        Trim: 'H1T - Jet Black Cloth Interior Trim',
        Engine: 'LFV - Engine 4 cyl, 1.5L, Turbo',
        Transmission: 'MNH - Transmission, 6speed, automatic'
      }
    ];
  }

  printDoc($event) {
    const divTag = document.getElementById('printArea');
    const newTag = document.createElement('div');
    newTag.innerHTML = '<div style="display: block;width: 100%;background: #134373;"><img src="/assets/images/gm_logo.gif" class="logo"></div>';
    divTag.insertBefore(newTag, divTag.firstChild);
    _.each(divTag.getElementsByClassName('no-print'), function (tag: HTMLElement) {
      tag.style.display = 'none';
    });
    const indecator: HTMLElement = <HTMLScriptElement>divTag.getElementsByClassName('mat-expansion-indicator')[0];
    indecator.style.display = 'none';
    const innerHtml = document.getElementById('printArea').innerHTML;
    let printMailTemplate: string = '<html><head>' +
      '<link rel="stylesheet" href="../../assets/print.css">' +
      '<link rel="stylesheet" href="../../assets/indigo-pink.css">' +
      '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">' +
      '</head>' +
      '<body>{{content}}</body></html>';
    printMailTemplate = printMailTemplate.replace('{{content}}', innerHtml);

    const printWin = window.open();
    printWin.document.write(printMailTemplate);
    printWin.focus();
    setTimeout(() => {
      indecator.style.display = '';
      divTag.firstElementChild.remove();
      _.each(divTag.getElementsByClassName('no-print'), function (tag: HTMLElement) {
        tag.removeAttribute('style');
      });
      printWin.print();
      printWin.close();
    }, 300);
  }

  printInvoice() {
    const invoices = response.getVehicleInvoices;
    const invoiceDoc = invoices.payload.content[0].showVehicleInvoice.showVehicleInvoiceDataArea.vehicleInvoice[0].invoice[0].invoiceDocText.value;
    this.printService.printInvoice(invoiceDoc);
  }

  printAllInvoice() {
    const invoices = response.getVehicleInvoices;
    let invoiceDoc = invoices.payload.content[0].showVehicleInvoice.showVehicleInvoiceDataArea.vehicleInvoice[0].invoice[0].invoiceDocText.value;
    invoiceDoc += `\n\n\n\n\n\n\n\n\n\n\n${invoiceDoc}`;
    this.printService.printInvoice(invoiceDoc);
  }

  showDifferentOptions() {
    this.differentOptions = !this.differentOptions;
  }

  showSimilarOptions() {
    this.similarOptions = !this.similarOptions;
  }
}
