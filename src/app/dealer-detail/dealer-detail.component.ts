import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {response} from "../api-response/response";

@Component({
  selector: 'general-motor-dealer-detail',
  templateUrl: './dealer-detail.component.html',
  styleUrls: ['./dealer-detail.component.scss']
})
export class DealerDetailComponent implements OnInit {

  public bac: any;
  public data: any;
  public dealer: any;

  constructor(private activatedRoute: ActivatedRoute) {
    this.activatedRoute
      .queryParams
      .subscribe(params => {
        this.bac = params['bac'];
      });
    this.dealer = {
      'SellingSourceCode': '13',
      'VendorId': '114404',
      'DealerCode': '19464',
      'Type': 'dealer',
      'Name': 'WENTWORTH CHEVROLET CO.'
    }
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.bac = params.bac;
      this.data = response.tradeContactInfo.response;
    });
  }

}
