import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {ActivationEnd, Router} from '@angular/router';

import * as _ from 'lodash';
import {response} from '../../api-response/response';
import {VSSService} from "../../services/vss.services";

@Component({
  selector: 'general-motor-search-by-location',
  templateUrl: './search-by-location.component.html',
  styleUrls: ['./search-by-location.component.scss']
})
export class SearchByLocationComponent implements OnInit {

  @Input() modal: any;
  public divisions = ['All', 'GMC', 'Buick', 'Cadillac', 'Chevrolet'];
  public selectedDivision = 'All';
  public states: Array<any>;
  public searchResult = [];
  public searchCriteria: any;
  public result = false;
  public inValidRadius = false;
  public showQuickLinks: boolean;
  public options: FormGroup;


  constructor(fb: FormBuilder, router: Router, titleService: Title, private vssService: VSSService) {
    titleService.setTitle('Locate Dealer');
    router.events.subscribe(event => {
      if (event instanceof ActivationEnd) {
        this.showQuickLinks = event.snapshot.url[0].path.includes('popup');
      }
    });
    this.options = fb.group({
      selectedSearch: 'Dealer Name',
      'Dealer Name': fb.group({
        name: new FormControl(''),
        state: new FormControl('', Validators.required)
      }),
      'Zip Code': fb.group({
        code: new FormControl({value: '', disabled: true}),
        radius: new FormControl({value: '', disabled: true})
      }),
      'City': fb.group({
        city: new FormControl({value: '', disabled: true}),
        state: new FormControl({value: '', disabled: true}),
        radius: new FormControl({value: '', disabled: true})
      }),
      BAC: fb.group({
        number: new FormControl({value: '', disabled: true})
      })
    });
  }

  ngOnInit() {
    //get All states
    /*this.vssService.getStates().subscribe(response => {
    });*/
    this.states = response.getStates;
  }

  changeRadio() {
    let searchOption = '';

    _.each(this.options.controls, (control: any, index) => {
      if (index === 'selectedSearch') {
        searchOption = control.value;
      } else {
        if (index !== searchOption) {
          _.forEach(control.controls, (value) => {
            value.validator = null;
            value.disable();
          });
        } else {
          _.forEach(control.controls, (value, field) => {
            if (field === 'city' || field === 'state') {
              value.validator = Validators.required;
            }
            if (field === 'number') {
              value.setValidators([Validators.required, Validators.minLength(6), Validators.maxLength(6)]);
            }
            if (field === 'radius') {
              value.setValidators([Validators.required, Validators.max(500)]);
            }
            if (field === 'code') {
              value.setValidators([Validators.required, Validators.minLength(5), Validators.maxLength(5)]);
            }
            value.enable();
          });
        }
      }
    });
  }

  checkNumber(event, field) {
    if (field === 'radius') {
      if (_.parseInt(event.target.value) > 500) {
        this.inValidRadius = true;
      }
      return event.charCode >= 48 && event.charCode <= 57 && event.target.value.length < 3;
    }
    return event.charCode >= 48 && event.charCode <= 57;
  }

  editSearch() {
    this.result = false;
  }

  showDealerDetail(selectedSearch) {
    if (selectedSearch.valid) {
      this.result = true;
      this.searchCriteria = selectedSearch.value;
      this.searchResult = [{
        BAC: '1122',
        dealerName: 'rinki',
        owingDealerCode: '132',
        Address: 'sgdhgsd,dgdfg,dffgdjsf',
        phone: '123564987',
        fax: '899874654'
      }, {
        BAC: '152',
        dealerName: 'vnbn',
        owingDealerCode: 'bvnvbn',
        Address: 'vbnvbn',
        phone: '6565',
        fax: '56465'
      }, {
        BAC: '1122',
        dealerName: 'rinki',
        owingDealerCode: 'rgghth',
        Address: 'jujuju',
        phone: 'ujuj',
        fax: 'ukukkk'
      }, {
        BAC: '1122',
        dealerName: 'nikita',
        owingDealerCode: '6h7j7',
        Address: 'j77j7',
        phone: 'jjk7k7',
        fax: '7k7k7k'
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }, {
        BAC: '1122',
        dealerName: '',
        owingDealerCode: '',
        Address: '',
        phone: '',
        fax: ''
      }];
    }
  }

  validateRadius(value) {
    this.inValidRadius = (value > 500);
  }
}
