import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class VSSService {

	constructor(private http: Http) {
	}


  //auto Login
  /*autoLogin(){
    return this.http.get('https://demo1.dealerpeak.net/admin', {withCredentials: true});
  }*/
  /*autoLogin() {
    return this.http.get('https://gmcertification.dealerpeak.net/?fuseaction=agent.default&rid=9',{withCredentials: true})
      .map((response: Response) => {
      })
  }*/

  //get All States
  getStates() {
    /*function createCORSRequest(method, url){
      const xhr = new XMLHttpRequest();
      if ("withCredentials" in xhr){
        xhr.open(method, url, true);
      } else if (typeof XDomainRequest != "undefined"){
        xhr = new XDomainRequest();
        xhr.open(method, url);
      } else {
        xhr = null;
      }
      return xhr;
    }

    const request = createCORSRequest("get", "https://gmcertification.dealerpeak.net/REST/locale/state");
    if (request){
      request.onload = function(response){
        debugger
        //do something with request.responseText
      };
      request.send();
    }*/
    return this.http.get('https://gmcertification.dealerpeak.net/REST/locale/state')
      .map((response: Response) => {
        return response.json();
      })
      /*.catch((error: Response) => {
        //return Observable.of({error: true, err: error.json()});
      });*/
  }

  // get List of All Years
  getAllYears() {
    return this.http.get('https://gmcertification.dealerpeak.net/REST/gm/premium/vss/years?locationID=2543&yearStart=2013&yearEnd=2017')
      .map((response: Response) => {
        return response.json().ModelYears;
      })
      /*.catch((error: Response) => {
       // return Observable.of({error: true, err: error.json()});
      });*/
  }

	// git List Of All Makes of specified year
	getAllMakes(year) {

		/*return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vss/years/${year}/makes`)
			.map((response) => {
				return response.json();
			});*/
	}

	// get List Of All Models for specific year and given makeId
	getAllModels(year, makeId) {
		return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vss/years/${year}/makes/${makeId}/models`)
			.map((response) => {
				return response.json();
			});
	}

	// get List of all years with all makes and models
	getAllYearMakeModel(year) {
		return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vss/year-make-models?locationID=2543&yearStart=2013&yearEnd=2017`)
			.map((response) => {
				return response.json();
			});
	}

	// get model option group for selected model
	getModelOptionGroup(year, makeId, modelId) {
		return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vss/years/${year}/makes/${makeId}/models/${modelId}/optiongroups`)
			.map((response) => {
				return response.json();
			});
	}
}
