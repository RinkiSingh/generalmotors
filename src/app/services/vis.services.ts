import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class VISService {

  constructor(private http: Http) {
  }

  getVehicleInvoiceDocs(invoiceNumber) {
    return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vis/invoices/${invoiceNumber}?locationID=2543`)
      .map((response) => {
        return response.json();
      });
  }

  getVehicleInvoices(vinNumber) {
    return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vis/invoices?locationID=2543&original_document=true&vin=${vinNumber}`)
      .map((response) => {
        return response.json();
      });
  }
}
