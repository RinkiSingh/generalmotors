import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class VLSService {

	constructor(private http: Http) {
	}


	// get List of Vehicles by PostalCode
	locateVehiclesByPostalCode(makeCode, modelCode, postalCode, radius, year) {
		return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vls/vehicle-by-postal-code?makecode=${makeCode}&modelcode=${modelCode}&postalcode=${postalCode}&radius=${radius}&year=${year}`)
			.map((response) => {
				return response.json();
			});
	}

	// get List of Vehicles by VIN
	locateVehicleByVin(vinNumber) {
		return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vls/vehicle-by-vin/${vinNumber}`)
			.map((response) => {
				return response.json();
			});
	}

  // get List of Vehicles by City
  locateVehiclesByCity(city, makeCode, modelCode, radius, state, year) {
    return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vls/vehicle-by-city?city=${city}&makecode=${makeCode}&modelcode=${modelCode}&radius=${radius}&state=${state}&year=${year}`)
      .map((response) => {
        return response.json();
      });
  }

  // get List of Vehicles by State
  locateVehiclesByRegion(makeCode, modelCode, radius, states, year) {
    return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vls/vehicle-by-region?makecode=${makeCode}&modelcode=${modelCode}&radius=${radius}&region=${states}&year=${year}`)
      .map((response) => {
        return response.json();
      });
  }

  // get List of Vehicles by Vendor(BAC)
  locateVehiclesByVendor(BAC, makeCode, modelCode, radius, year) {
    return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vls/vehicle-by-vendor?istradingpartnersearch=false&makecode=${makeCode}&modelcode=${modelCode}&radius=5&vendorid=${BAC}&year=${year}`)
      .map((response) => {
        return response.json();
      });
  }

  // get List of Vehicles by Single Vendor(BAC)
  locateVehiclesBySingleVendor(BAC, makeCode, modelCode, radius, year) {
    return this.http.get(`https://gmcertification.dealerpeak.net/REST/gm/premium/vls/vehicle-by-vendor?istradingpartnersearch=false&makecode=${makeCode}&modelcode=${modelCode}&radius=5&vendorid=${BAC}&year=${year}`)
      .map((response) => {
        return response.json();
      });
  }
}
