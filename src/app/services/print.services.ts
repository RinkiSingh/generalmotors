import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class PrintService {

  constructor() {
  }

  printInvoice(invoiceDoc) {
    /*invoiceDoc = invoiceDoc.replace(/\n/g, '<br/>');
     const win = window.open('', '_blank', 'location=yes,scrollbars=yes,status=yes');
     document.getElementById('inputInvoice').innerHTML = invoiceDoc;
     document.getElementById('inputInvoice').setAttribute('style', 'font-size: 9px');
     document.getElementById('printInvoice').setAttribute('style', 'display: inline-table');
     html2canvas(document.getElementById('printInvoice')).then((canvas: any) => {
     const data = canvas.toDataURL();
     const docDefinition = {
     info: {
     title: 'Invoice'
     },
     content: [{
     image: data
     }]
     };
     pdfMake.createPdf(docDefinition).open({}, win);
     document.getElementById('printInvoice').setAttribute('style', 'display: none');
     });*/
    invoiceDoc = invoiceDoc.replace(/\n/g, '<br/>');
    let printMailTemplate: string = '<html><head>' +
      '</head>' +
      '<body><pre>{{content}}</pre></body></html>';
    printMailTemplate = printMailTemplate.replace('{{content}}', invoiceDoc);

    const printWin = window.open();
    printWin.document.write(printMailTemplate);
    printWin.focus();
    setTimeout(() => {
      printWin.print();
      printWin.close();
    }, 300);
  }
}
