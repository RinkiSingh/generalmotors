import {Component} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'general-motor-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public showHeader = false;
  public showRoute = false;
  public showHelp = false;

  constructor(router: Router, titleService: Title) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const title = this.getTitle(router.routerState, router.routerState.root).join('-');
        this.showHeader = event.url === '/' || event.url.startsWith('/locate') || event.url.startsWith('/popup');
        this.showRoute = event.url === '/' || event.url.startsWith('/locate');
        this.showHelp = event.url === '/' || event.url.startsWith('/locate');
        titleService.setTitle(title);
      }
    });
    /*document.cookie = "JSESSIONID=A3B83BA24FEECAD9F0F03F216C2FFAA8.InstanceA2;";
    document.cookie = "DPAffinity=2fa125897de9b11400a482e24c92032e7c2b14f3d647c99fe2eac3a2131a5416;";
    document.cookie = "INSTANCE=InstanceA2;";
    document.cookie = "ZIPCODE_CLASSID=%20;";
    document.cookie = "TIMESTAMP=%7Bts%20%272017%2D12%2D27%2023%3A16%3A57%27%7D;";
    document.cookie = "USERID=17CBADE2%2DA4DE%2DEA54%2D1E7C%2D2BA74EBFA229;";
    document.cookie = "INTERFACEID=1;";
    document.cookie = "_ga=GA1.2.278892454.1514445420;";
    document.cookie = "_gid=GA1.2.2071120933.1514445420;";
    document.cookie = "_gat=1;";*/
  }

  title = 'general-motor';

  getTitle(state, parent) {
    const data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }
    return data;
  }
}
