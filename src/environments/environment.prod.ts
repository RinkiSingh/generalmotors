export const environment = {
  production: true,
  baseURL: 'https://gm.plus.dealerpeak.com/#',
  locateDealer: 'locate-dealer',
  locateVehicle: 'locate-vehicle',
  vinDetail: 'vin-detail',
  viewReport: 'view-report',
  compareVehicle: 'compare-vehicle',
  tradingPartner: 'trading-partner',
  helpfulLink: 'popup/helpful-link',
  dealerDetail: 'popup/dealer-detail'
};
